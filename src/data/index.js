//Static Data for table and Chart.

//Table Data.
export const tableData = [
    {
        risk: 1, stock: 20, bonds: 30, etfs: 15, cash: 15, commodities: 10, fixed: 10
    },
    {
        risk: 2, stock: 40, bonds: 5, etfs: 10, cash: 18, commodities: 7, fixed: 20
    },
    {
        risk: 3, stock: 5, bonds: 10, etfs: 15, cash: 40, commodities: 23, fixed: 7,
    },
    {
        risk: 4, stock: 32, bonds: 8, etfs: 21, cash: 9, commodities: 10, fixed: 20,
    },
    {
        risk: 5, stock: 12, bonds: 18, etfs: 25, cash: 4, commodities: 16, fixed: 39,
    },
    {
        risk: 6, stock: 40, bonds: 15, etfs: 5, cash: 20, commodities: 6, fixed: 14,
    },
    {
        risk: 7, stock: 20, bonds: 10, etfs: 10, cash: 30, commodities: 15, fixed: 5,
    },
    {
        risk: 8, stock: 14, bonds: 6, etfs: 30, cash: 10, commodities: 25, fixed: 15,
    },
    {
        risk: 9, stock: 8, bonds: 10, etfs: 12, cash: 30, commodities: 10, fixed: 30,
    },
    {
        risk: 10, stock: 26, bonds: 18, etfs: 15, cash: 24, commodities: 14, fixed: 3,
    }];

//Table Columns.
export const TableColumns = [
    {
        Header: "Risk Level", accessor: 'risk'
    },
    {
        Header: "Stocks", accessor: 'stock'
    },
    {
        Header: "Bonds", accessor: 'bonds'
    },
    {
        Header: "ETFs", accessor: 'etfs'
    },
    {
        Header: "Cash", accessor: 'cash'
    },
    {
        Header: "Commodities", accessor: 'commodities'
    },
    {
        Header: "Fixed Income", accessor: 'fixed'
    }
]

//Char Labels.
export const chartLabels = ['Stocks', 'Bonds', 'ETFs', 'Cash', 'Commodities', 'Fixed Income'];

//Chart Colors.
export const chartColors = [
    'rgba(51, 102, 204, .7)',
    'rgba(220, 57, 18, .7)',
    'rgba(255, 153, 0, .7)',
    'rgba(16, 150, 24, .7)',
    'rgba(153, 0, 153, .7)',
    'rgba(59, 62, 172, .7)'
];