//Libraries.
import React, { Component } from 'react';

//Data.
import { tableData } from '../../data'

//Components.
import Header from '../Header';
import Chart from '../Chart';
import SliderControl from '../SliderControl';
import Table from '../Table';

class App extends Component {
  tableData = tableData;

  constructor() {
    super();
    const initRisk = 1;
    this.state = {
      riskLevel: initRisk,
      chartDatasets : this.getTableItems(initRisk, tableData),
      isDoughnut: true
    }
  }

  //Update chart.
  handleChange = newRisk => {
    if (newRisk !== this.state.riskLevel) {
      this.setState({
        riskLevel: newRisk,
        chartDatasets: this.getTableItems(newRisk, this.tableData)
      })
    }
  };

  //Get data from table items.
  getTableItems(index, elements) {
    let items = Object.values(elements[index - 1]);
    items.shift();
    return items;
  }

  //Change Chart.
  changeChart = (e) => {
    this.setState({
      isDoughnut: e.target.value === 'Doughnut' ? true : false
    })
  }

  render() {
    //Get vars from state.
    const { riskLevel, chartDatasets, isDoughnut } = this.state;
    return (
      <div>
        <Header changeChart={this.changeChart} />
        <div className="container">
          <div className="row">
            <div className="col-9">
              <Chart chartDatasets={chartDatasets} isDoughnut={isDoughnut} />
            </div>

            <div className="col-3">
              <SliderControl
                riskLevel={riskLevel}
                handleChange={this.handleChange} />
            </div>

            <div className="col-12">
              <Table tableData={this.tableData} index={riskLevel} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
