//Libraries.
import React from 'react';
import PropTypes from 'prop-types';
import { Doughnut, Pie } from 'react-chartjs-2'

//Data.
import { chartLabels, chartColors } from '../../data'

const Chart = (props) => {

    //Set data for Doughnut component.
    const chartData = {
        labels: chartLabels,
        datasets: [
            {
                data: props.chartDatasets,
                backgroundColor: chartColors
            }
        ]
    }
    //Set options for Doughnut component.
    const chartOptions = {
        legend: {
            position: 'left'
        },
        animation: {
            animateRotate: true,
            animateScale: true
        },

        tooltips: {

        }
    };

    return (
        <div>
            {props.isDoughnut ? (
                < Doughnut data={chartData} options={chartOptions} />
            ) : (
                    <Pie data={chartData} options={chartOptions} />
                )}
        </div>
    );
}

//Validate props.
Chart.propTypes = {
    chartDatasets: PropTypes.array.isRequired
};

export default Chart;