//Libraries.
import React from 'react';
import PropTypes from 'prop-types';

//Styles.
import './header.css'

const Header = (props) => {
    return (
        <div className="header">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h1>Investment Portafolio</h1>
                        <button className="doughnut" onClick={props.changeChart} value="Doughnut"> Doughnut </button>
                        <button className="pie" onClick={props.changeChart} value="Pie"> Pie </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

//Validate props.
Header.propTypes = {
    changeChart: PropTypes.func.isRequired,
};

export default Header;