//Libraries.
import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table'
import 'react-table/react-table.css'

//Data.
import { TableColumns } from '../../data'

//Styles.
import './table.css'

const Table = (props) => {
    return (
        <div>
            <ReactTable
                getTrProps={(state, rowInfo, column) => {
                    let _class = 'row-active';
                    return {
                        className: rowInfo.index === props.index - 1 ? _class : ''
                    }
                }}

                data={props.tableData}
                columns={TableColumns}
                defaultPageSize={10}
                showPagination={false}
                sortable={false}
            />
            <div className="table-info"><i>% of risk per instrument.</i></div>
        </div>
    );
}

//Validate props.
Table.propTypes = {
    tableData: PropTypes.array.isRequired
};

export default Table;