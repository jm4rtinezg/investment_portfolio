//Libraries.
import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-rangeslider';

//Styles.
import './slider.css'

const SliderControl = (props) => {
    return (
        <div className="slider-container">
            <h4>Risk tolerence  <span>{props.riskLevel}</span></h4>
            <div className="content">
                <div className="slider orientation-reversed">
                    <div className="slider-group">
                        <div className="slider-horizontal">
                            <Slider
                                min={1}
                                max={10}
                                value={props.riskLevel}
                                orientation='horizontal'
                                onChange={props.handleChange}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

//Validate props.
SliderControl.propTypes = {
    riskLevel: PropTypes.number.isRequired,
    handleChange: PropTypes.func.isRequired
};

export default SliderControl;