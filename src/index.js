//Libraries.
import React from 'react';
import {render} from 'react-dom';

//Component.
import App from './components/App';

//Style
import './libs/global.css';

//Render.
render(<App />, document.getElementById('root'));
