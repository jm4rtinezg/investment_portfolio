# Investment Portafolio.

App that shows (Doughnut and Pie) charts representing the percentage risk of different financial instruments.

### Prerequisites

You’ll need to have Node >= 6 on your local development machine. (but it’s not required on the server).

### Installing

1. Clone (git clone https://bitbucket.org/jm4rtinezg/investment_portfolio).
2. Enter to investment_portfolio folder.
2. Run npm install to download dependencies.
3. Run npm start to open the app in http://localhost:3000/

## Built With
* [Create React apps CLI] (https://github.com/facebookincubator/create-react-app) - Create React apps with no build configuration.


## Third Party Tools

* [Reactjs](https://github.com/facebook/react) - A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [React Table](https://github.com/react-tools/react-table) - React Table - A lightweight, fast and extendable datagrid for React.
* [React Ranges Slider](https://github.com/whoisandy/react-rangeslider) - A lightweight responsive react range slider component.
* [React Chartjs 2](https://github.com/jerairrest/react-chartjs-2) - React wrapper for Chart.js


## Notes
1. Video in folder named (video).
2. This is a development version of the project, for get production version we need to run:

npm run build